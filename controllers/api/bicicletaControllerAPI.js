var Bicielectrica = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicielectrica.allBicis
    });
}

exports.bicicleta_create = function(req, res){
    var bici = new Bicielectrica(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicielectrica.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = function(req, res){
    Bicielectrica.removeById(req.body.id);
    res.status(204).send();
}