var Bicielectrica = require('../../models/bicicleta');
beforeEach(() => { Bicielectrica.allBicis = []; });


describe('Bicicleta.allBicis', () => {
    it('comienza vacía', () => {
        expect(Bicielectrica.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicielectrica.allBicis.length).toBe(0);

        var a = new Bicielectrica(1, 'rojo', 'urbana', [-31.620428, -60.686190]);
        Bicielectrica.add(a);

        expect(Bicielectrica.allBicis.length).toBe(1);
        expect(Bicielectrica.allBicis[0]).toBe(a);

    })
});

describe('Bicielectrica.findById', () => {
    it('devolver la bici con id 1', () => {
        expect(Bicielectrica.allBicis.length).toBe(0);
        var aBici1 = new Bicielectrica(1, 'verde', 'urbana');
        var aBici2 = new Bicielectrica(2, 'roja', 'montaña');
        Bicielectrica.add(aBici1);
        Bicielectrica.add(aBici2);

        var targetBici = Bicielectrica.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici1.color);
        expect(targetBici.modelo).toBe(aBici1.modelo);
    });
});


describe('Bicielectrica.removeById', () => {
    it('remover la bici con id 1', () => {
        var aBici1 = new Bicielectrica(1, 'verde', 'urbana');
        var aBici2 = new Bicielectrica(2, 'roja', 'montaña');
        Bicielectrica.add(aBici1);
        Bicielectrica.add(aBici2);

        var targetBici = Bicielectrica.removeById(1);
        expect(targetBici).toBe(undefined);
    });
});
/*
Bicielectrica.removeById = function(aBiciId){
    for(var i=0; i < Bicielectrica.allBicis.length; i++){
        if (Bicielectrica.allBicis[i].id == aBiciId){
            Bicielectrica.allBicis.splice(i, 1);
            break;
        }
    }
}*/