var Bicielectrica = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
const { findById } = require('../../models/bicicleta');

describe('Bicielectrica API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicielectrica.allBicis.length).toBe(0);

            var a = new Bicielectrica(1, 'negro', 'urbana', [-31.646928, -60.705837]);
            Bicielectrica.add(a);

            request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicielectrica.findById(10).color).toBe("rojo");
                done();
            });
        });
    });
});