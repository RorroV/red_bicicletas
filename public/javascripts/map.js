var map = L.map('main_map').setView([-31.6225960,-60.74682], 12.5);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var biciIcon = L.icon({
    iconUrl: '../assets/img/bici-electrica.png',

    iconSize:     [30, 40], // size of the icon
    iconAnchor:   [13, 13], // point of the icon which will correspond to marker's location
    popupAnchor:  [-3, -36] // point from which the popup should open relative to the iconAnchor
});

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {icon: biciIcon}).addTo(map).bindPopup(`Estación de bicis eléctricas nº${bici.id}.`);
        });
    }
})