var Bicielectrica = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicielectrica.prototype.toString = function (){
    return 'id: ' + this.id + "| color: " + this.color;
}

Bicielectrica.allBicis = [];
Bicielectrica.add = function(aBici){
    Bicielectrica.allBicis.push(aBici);
}

Bicielectrica.findById = function(aBiciId){
    var aBici = Bicielectrica.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

Bicielectrica.removeById = function(aBiciId){
    for(var i=0; i < Bicielectrica.allBicis.length; i++){
        if (Bicielectrica.allBicis[i].id == aBiciId){
            Bicielectrica.allBicis.splice(i, 1);
            break;
        }
    }
}
/*
var a = new Bicielectrica(1, 'rojo', 'urbana', [-31.620428, -60.686190]);
var b = new Bicielectrica(2, 'blanca', 'urbana', [-31.632127, -60.705193]);
var c = new Bicielectrica(3, 'roja', 'urbana', [-31.646928, -60.705837]);
var d = new Bicielectrica(4, 'blanca', 'urbana', [-31.646138, -60.718716]);

Bicielectrica.add(a);
Bicielectrica.add(b);
Bicielectrica.add(c);
Bicielectrica.add(d);
*/
module.exports = Bicielectrica;